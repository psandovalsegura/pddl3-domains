; Fetch Objects Domain

(define (domain fetchobjects)
    (:requirements :typing :fluents)
    (:types robot target charger location - object)

    ;
    ; Predicates
    ;

    (:predicates
        (robot-at ?rob - robot ?loc - location)
        (target-collected ?tar - target)
        (target-at ?tar - target ?loc - location)
        (charger-at ?cgr - charger ?loc - location)
        (robot-holding ?rob - robot)
        (not-robot-holding ?rob - robot)
        (adjacent ?loc1 - location ?loc2 - location)
    )


    ;
    ; Functions
    ;

    (:functions
        (battery-level ?rob - robot)
    )

    ;
    ; Actions
    ;

    ; Move robot from current location to next location
    (:action move
        :parameters (?rob - robot ?cur ?nxt - location)
        :precondition (and (robot-at ?rob ?cur) (adjacent ?cur ?nxt) (> (battery-level ?rob) 0))
        :effect (and (not (robot-at ?rob ?cur)) (robot-at ?rob ?nxt) (decrease (battery-level ?rob) 1))
    )

    ; Collect target using robot at current location
    (:action collect-target
        :parameters (?rob - robot ?tar - target ?cur - location)
        :precondition (and (robot-at ?rob ?cur) (target-at ?tar ?cur) (not-robot-holding ?rob))
        :effect (and (target-collected ?tar) (not (target-at ?tar ?cur)))
    )

    ; Pick up charger
    (:action pick-up-charger
        :parameters (?rob - robot ?cgr - charger ?cur - location)
        :precondition (and (robot-at ?rob ?cur) (charger-at ?cgr ?cur) (not-robot-holding ?rob))
        :effect (and (not (charger-at ?cgr ?cur)) (robot-holding ?rob) (not (not-robot-holding ?rob)) (assign (battery-level ?rob) 4))
    )

    ; Put down charger
    (:action put-down-charger
        :parameters (?rob - robot ?cgr - charger ?cur - location)
        :precondition (and (robot-at ?rob ?cur) (robot-holding ?rob))
        :effect (and (charger-at ?cgr ?cur) (not (robot-holding ?rob)) (not-robot-holding ?rob))
    )
)
