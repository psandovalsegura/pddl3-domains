; Simple Robot Domain

(define (domain simplerobot)
    (:requirements :typing :constraints)

    (:types 
        robot location - object
        destination waypoint - location
    )
    
    (:predicates
        (robot-at ?r - robot ?l - location)
        (adjacent ?l1 - location ?l2 - location)
    )

    (:action move
        :parameters (?r - robot ?cur ?nxt - location)
        :precondition (and (robot-at ?r ?cur) (adjacent ?cur ?nxt))
        :effect (and (not (robot-at ?r ?cur)) (robot-at ?r ?nxt))
    )
)
