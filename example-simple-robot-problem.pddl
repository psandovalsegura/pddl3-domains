; Simple Robot Problem

(define
    (problem simplerobot-problem)
    (:domain simplerobot)

    (:objects
        l1 l2 l3 l4 - location
        r1 - robot
        d1 d2 - destination
        w1 w2 - waypoint
    )

    (:init
        (adjacent d1 w1) (adjacent w1 d1)
        (adjacent w1 w2) (adjacent w2 w1)
        (adjacent w2 d2) (adjacent d2 w2)
        (robot-at r1 d2)
    )

    (:goal
        (and
            (robot-at r1 d1)
        )
    )
    
    ; Constraints are expressed through temporal modal operators,
    ; akin to operators in LTL, over first-order formula using domain
    ; predicates. The basic modal operators are:
    ;
    ; 1. (always <condition>)
    ; 2. (sometime <condition>)
    ; 3. (at-most-once <condition>)
    ; 4. (at-end <condition>)
    ; 5. (within <condition>)
    ; 6. (sometime-after <condition1> <condition2>)
    ; 7. (sometime-before <condition1> <condition2>)
    ; 8. (always-within <num> <condition1> <condition2>)
    
    (:constraints
		(sometime-after (robot-at r1 w2) (robot-at r1 w1))
    )
)
