; Explore Domain

(define (domain explore)
    (:requirements :typing)
    (:types ground-vehicle air-vehicle information equipment charger location - object)

    ;
    ; Predicates
    ;

    (:predicates
        (ground-vehicle-at ?ugv - ground-vehicle ?loc - location)
        (air-vehicle-at ?uav - air-vehicle ?loc - location)
        (information-collected ?info - information)
        (information-at ?info - information ?loc - location)
        (equipment-collected ?eqp - equipment)
        (equipment-at ?eqp - equipment ?loc - location)
        (charger-at ?cgr - charger ?loc - location)
        (air-vehicle-holding-charger ?ugv - ground-vehicle ?cgr - charger)
        (ground-vehicle-holding-charger ?uav - air-vehicle ?cgr - charger)
        (adjacent ?loc1 - location ?loc2 - location)
        (is-base ?loc - location)
    )

    ;
    ; Actions
    ;

    ; Move UGV from current location to next location
    (:action move-ugv
        :parameters (?ugv - ground-vehicle ?cur ?nxt - location)
        :precondition (and (ground-vehicle-at ?ugv ?cur) (adjacent ?cur ?nxt))
        :effect (and (not (ground-vehicle-at ?ugv ?cur)) (ground-vehicle-at ?ugv ?nxt))
    )

    ; Move UAV from current location to next location
    (:action move-uav
        :parameters (?uav - air-vehicle ?cur ?nxt - location)
        :precondition (and (air-vehicle-at ?uav ?cur) (adjacent ?cur ?nxt))
        :effect (and (not (air-vehicle-at ?uav ?cur)) (air-vehicle-at ?uav ?nxt))
    )

    ; Collect information using vehicle at current location

    ; Collect equipment using vehicle at current location

    ; Drop off information at base

    ; Pick up charger

    ; Put down charger
   
)
