# pddl3-domains
Implementations of common domains to explore LTL-like planning in PDDL3.

### Requirements
- The [OPTIC temporal planner](https://nms.kcl.ac.uk/planning/software/optic.html)

### How to generate plans
```
optic-clp domain.pddl problem.pddl
```