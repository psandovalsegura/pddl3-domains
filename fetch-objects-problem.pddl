; Fetch Objects Problem

(define
    (problem fetchobjects-problem)
    (:domain fetchobjects)

    (:objects
        r1 - robot
        t1 t2 - target
        c1 c2 - charger
        l1 l2 l3 l4 l5 l6 l7 l8 l9 - location
    )

    (:init
        (adjacent l1 l2) (adjacent l1 l4) (adjacent l2 l3) (adjacent l2 l5) (adjacent l3 l6)
        (adjacent l2 l1) (adjacent l4 l1) (adjacent l3 l2) (adjacent l5 l2) (adjacent l6 l3)
        (adjacent l4 l5) (adjacent l5 l6) (adjacent l4 l7) (adjacent l5 l8) (adjacent l6 l9) 
        (adjacent l5 l4) (adjacent l6 l5) (adjacent l7 l4) (adjacent l8 l5) (adjacent l9 l6) 
        (adjacent l7 l8) (adjacent l8 l9) 
        (adjacent l8 l7) (adjacent l9 l8)
        (robot-at r1 l1)
        (= (battery-level r1) 4)
        (not-robot-holding r1)
        (not (robot-holding r1))
        (charger-at c1 l3) (charger-at c2 l9)
        (target-at t1 l6) (target-at t2 l7)
    )

    (:goal
        (and
            (target-collected t1)
            (target-collected t2)
        )
    )
)
